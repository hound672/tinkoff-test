FROM python:3.7 AS base
RUN pip install pipenv
WORKDIR /app

FROM base AS dependencies
COPY src/Pipfile .
RUN pipenv install

FROM dependencies AS build
WORKDIR /app
COPY src .
